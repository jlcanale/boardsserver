import Vapor

var env = try Environment.detect()
try LoggingSystem.bootstrap(from: &env)
let app = Application(env)
defer { app.shutdown() }
try configure(app)
try PushNotificationHelper.configureAPNS(app)
print("Routes: \(app.routes.all)")
try app.run()
