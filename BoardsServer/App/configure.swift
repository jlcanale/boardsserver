import Fluent
import FluentPostgresDriver
import Vapor

// configures your application
public func configure(_ app: Application) throws {
    // uncomment to serve files from /Public folder
    // app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))

    app.http.server.configuration.port = 6060
    app.http.server.configuration.hostname = "0.0.0.0"

    app.databases.use(.postgres(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        username: Environment.get("DATABASE_USERNAME") ?? "boards",
        password: Environment.get("DATABASE_PASSWORD") ?? "boards",
        database: Environment.get("DATABASE_NAME") ?? "boards"
    ), as: .psql)

    _ = app.autoMigrate()
    app.migrations.add(UserMigration())
    app.migrations.add(FriendMigration())
    app.migrations.add(DeviceMigration())
    app.migrations.add(TokenMigration())

    // register routes
    try routes(app)
}
