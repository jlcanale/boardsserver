//
//  PushNotificationHelper.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/13/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Vapor
import APNS
import JWTKit

class PushNotificationHelper {

    struct BasicNotification: APNSwiftNotification {
        let aps: APNSwiftPayload
    }

    static let shared = PushNotificationHelper()

    static private let apnsPrivateKeyFilePath = "/usr/local/etc/BoardsServer/AuthKey_UTD8HH93Y9.p8"
    static private let apnsKeyIdentifier = JWKIdentifier(string: "UTD8HH93Y9")
    static private let apnsTeamIdentifier = "P2B5K93MR3"
    static private let apnsTopic = "com.CanaleCoding.Boards"
    private let group = MultiThreadedEventLoopGroup(numberOfThreads: 1)

    static func configureAPNS(_ app: Application) throws {
        let config = try APNSwiftConfiguration(authenticationMethod: .jwt(key: .private(filePath: apnsPrivateKeyFilePath), keyIdentifier: apnsKeyIdentifier,teamIdentifier: apnsTeamIdentifier), topic: apnsTopic, environment: .sandbox)
        app.apns.configuration = config
    }

    static func send(_ boothBroadcast: BoothBroadcast, for request: Request) throws -> EventLoopFuture<HTTPStatus> {
        User.findByAppleUserID(boothBroadcast.sender, on: request.db).unwrap(or: Abort(.notFound)).map { sender in
            let notification = BoothBroadcastNotification(boothBroadcast, from: sender)

            for friendKey in boothBroadcast.recipients {
                _ =  User.findByFriendKey(friendKey, on: request.db).unwrap(or: Abort(.notFound)).map { user in
                    for device in user.devices {
                        _ = request.apns.send(notification, to: device.deviceID)
                    }
                }
            }
        }.transform(to: .ok)
    }

    static func send(_ boothBroadcastResponse: BoothBroadcastResponse, for request: Request) throws -> EventLoopFuture<HTTPStatus> {
        User.findByAppleUserID(boothBroadcastResponse.sender, on: request.db).unwrap(or: Abort(.notFound)).map { sender in
            let notification = BoothBroadcastResponseNotification(boothBroadcastResponse, from: sender)

            for friendKey in boothBroadcastResponse.recipients {
                _ = User.findByFriendKey(friendKey, on: request.db).unwrap(or: Abort(.notFound)).map { user in
                    for device in user.devices {
                        _ = request.apns.send(notification, to: device.deviceID)
                    }
                }
            }
        }.transform(to: .ok)
    }
}

extension URL {
    var absoluteStringWithoutScheme: String {
        if absoluteString.starts(with: "file://") {
            return absoluteString.replacingOccurrences(of: "file://", with: "")
        } else {
            return absoluteString
        }
    }
}
