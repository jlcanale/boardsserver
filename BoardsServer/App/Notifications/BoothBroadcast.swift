//
//  BoothBroadcast.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/14/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import APNSwift
import Foundation

struct BoothBroadcast: Codable {
    var boothNumber: String
    var sender: String
    var recipients: [String]
    var gameTitle: String?
}


struct BoothBroadcastResponse: Codable {
    enum Response: String, Codable {
        case onMyWay = "ON_MY_WAY"
        case decline = "DECLINE"
    }

    var response: Response
    var threadID: String
    var sender: String
    var recipients: [String]
}

struct BoothBroadcastNotification: APNSwiftNotification {
    let fromFriendKey: String
    let boothNumber: String
    let aps: APNSwiftPayload

    init (_ boothBroadcast: BoothBroadcast, from user: User) {
        let threadID = UUID().uuidString
        self.fromFriendKey = boothBroadcast.sender
        self.boothNumber = boothBroadcast.boothNumber

        var body = "I'm at booth \(boothBroadcast.boothNumber)"
        if let gameTitle = boothBroadcast.gameTitle {
            body.append(" checking out \(gameTitle)")
        }

        let alert = APNSwiftPayload.APNSwiftAlert(title: "\(user.firstName) \(user.lastName) Says:", body: body)
        aps = APNSwiftPayload(alert: alert, sound: .normal("DiceRoll.wav"), hasContentAvailable: true, category: Constants.RemoteNotifications.boothLocation.rawValue, threadID: threadID)

    }
}

struct BoothBroadcastResponseNotification: APNSwiftNotification {
    let aps: APNSwiftPayload

    init(_ boothBroadcastResponse: BoothBroadcastResponse, from user: User) {
        let alert = APNSwiftPayload.APNSwiftAlert(title: "\(user.firstName) \(user.lastName) Says:", body: "I'm on my way")
        aps = APNSwiftPayload(alert: alert, sound: .normal("DiceRoll.wav"), hasContentAvailable: true, category: Constants.RemoteNotifications.boothLocationResponse.rawValue, threadID: boothBroadcastResponse.threadID)
    }
}

