//
//  User.swift
//  App
//
//  Created by Joseph Canale on 5/1/20.
//

import Vapor
import Fluent

final class User: Model, Content {
    static let schema = "Users"

    struct Public: Content {
        let id: UUID
    }

    init() {
    }

    @ID(key: .id)
    var id: UUID?

    @Field(key: "FirstName")
    var firstName: String

    @Field(key: "LastName")
    var lastName: String

    @Field(key: "FriendKey")
    var friendKey: String

    @Field(key: "EmailAddress")
    var emailAddress: String

    @Field(key: "AppleUserID")
    var appleUserID: String

    @Siblings(through: UserFriend.self, from: \.$user, to: \.$friend)
    var friends: [User]

    @Children(for: \.$user)
    var devices: [Device]

    @Children(for: \.$user)
    var tokens: [Token]

    init(id: UUID? = nil, firstName: String, lastName: String, emailAddress: String, appleUserID: String?, friendKey: String? = nil) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.emailAddress = emailAddress
        self.friendKey = ShortCodeGenerator.getCode(ofLength: 6)
    }
}

extension User {
    static func findByAppleUserID(_ appleUserID: String, on database: Database) -> EventLoopFuture<User?> {
        return User.query(on: database).filter(\.$appleUserID == appleUserID).first()
    }

    static func findByFriendKey(_ friendKey: String, on database: Database) -> EventLoopFuture<User?> {
        return User.query(on: database).with(\.$devices).filter(\.$friendKey == friendKey).first()
    }

    func createToken(source: SessionSource) throws -> Token {
        let calendar = Calendar(identifier: .gregorian)
        let expiryDate = calendar.date(byAdding: .year, value: 1, to: Date())

        return try Token(userID: requireID(), token: [UInt8].random(count: 16).base64, source: source, expiresAt: expiryDate)
    }

    func asPublic() throws -> Public {
        Public(id: try requireID())
    }
}

extension User: ModelAuthenticatable {

    static let usernameKey = \User.$appleUserID
    static let passwordHashKey = \User.$appleUserID

    func verify(password: String) throws -> Bool {
        return self.appleUserID == password
    }
}
