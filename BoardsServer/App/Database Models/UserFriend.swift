//
//  UserFriend.swift
//  App
//
//  Created by Joseph Canale on 5/12/20.
//

import Vapor
import Fluent

final class UserFriend: Model {
    static let schema = "UserFriend"

    init() {
    }

    @ID(key: .id)
    var id: UUID?

    @Parent(key: "UserID")
    var user: User

    @Parent(key: "FriendID")
    var friend: User

    init(userID: UUID, friendID: UUID) {
        self.$user.id = userID
        self.$friend.id = friendID
    }
}
