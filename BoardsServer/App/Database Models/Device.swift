//
//  Device.swift
//  APNS
//
//  Created by Joseph Canale on 5/13/20.
//

import Vapor
import Fluent

final class Device: Model {
    static let schema = "Devices"

    init() {
    }

    @ID(key: .id)
    var id: UUID?

    @Field(key: "DeviceID")
    var deviceID: String

    @Parent(key: "UserID")
    var user: User

    init(deviceID: String, userID: UUID) {
        self.deviceID = deviceID
        self.$user.id = userID
    }
}

extension Device {
    static func find(_ deviceID: String, on database: Database) -> EventLoopFuture<Device?> {
        return Device.query(on: database).filter(\.$deviceID == deviceID).first()
    }
}
