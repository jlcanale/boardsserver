//
//  Token.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/15/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Vapor
import Fluent

enum SessionSource: Int, Content {
    case signup
    case login
}

final class Token: Model {
    static let schema = "Tokens"

    init() {}

    init(id: UUID? = nil, userID: User.IDValue, token: String, source: SessionSource, expiresAt: Date?) {
        self.id = id
        self.$user.id = userID
        self.value = token
        self.source = source
        self.expiresAt = expiresAt
    }

    @ID(key: .id)
    var id: UUID?

    @Parent(key: "UserID")
    var user: User

    @Field(key: "Value")
    var value: String

    @Field(key: "Source")
    var source: SessionSource

    @Field(key: "ExpiresAt")
    var expiresAt: Date?

    @Timestamp(key: "CreatedDate", on: .create)
    var createdDate: Date?
}

extension Token: ModelTokenAuthenticatable {
    static let valueKey = \Token.$value
    static let userKey = \Token.$user

    var isValid: Bool {
        guard let expiresAt = self.expiresAt else { return true }
        return expiresAt > Date()
    }
}
