//
//  Constants.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/15/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Foundation

struct Constants {
    enum RemoteNotifications: String {
        case boothLocation = "BOOTH_LOCATION"
        case boothLocationResponse = "BOOTH_LOCATION_RESPONSE"
    }
}
