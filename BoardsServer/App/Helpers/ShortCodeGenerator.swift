//
//  ShortCodeGenerator.swift
//  BoardsServer
//
//  Created by Joseph Canale on 6/25/19.
//  Copyright © 2019 Joseph Canale. All rights reserved.
//

import Foundation

struct ShortCodeGenerator {
    
    private static let base62chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private static let maxBase : UInt32 = 62
    
    static func getCode(ofLength length: Int) -> String {
        var code = ""
        for _ in 0..<length {
            let random = Int.random(in: 0..<base62chars.count)
            let index = base62chars.index(base62chars.startIndex, offsetBy: random)
            code.append(base62chars[index])
        }
        return code
    }
}
