import Vapor

/// Register your application's routes here.
public func routes(_ app: Application) throws {
    // MARK: - User Routes
    let users = app.grouped("users")
    try users.register(collection: UserController())

    let notifications = app.grouped("notifications")
    try notifications.register(collection: APNSController())


}
