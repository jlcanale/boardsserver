//
//  UserError.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/15/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Vapor

enum UserError {
    case userAlreadyExists
}

extension UserError: AbortError {
    var description: String {
        reason
    }

    var status: HTTPResponseStatus {
        switch self {
            case .userAlreadyExists: return .conflict
        }
    }

    var reason: String {
        switch self {
            case .userAlreadyExists: return "User already exists."
        }
    }
}

