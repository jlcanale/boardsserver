//
//  TokenMigration.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/15/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Vapor
import Fluent

struct TokenMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Token.schema)
            .id()
            .field("UserID", .uuid, .references(User.schema, "id"))
            .field("Value", .string, .required).unique(on: "Value")
            .field("Source", .int, .required)
            .field("ExpiresAt", .datetime, .required)
            .field("CreatedDate", .datetime)
        .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Token.schema).delete()
    }
}
