//
//  FriendMigration.swift
//  APNS
//
//  Created by Joseph Canale on 5/13/20.
//

import Vapor
import Fluent

struct FriendMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(UserFriend.schema)
            .id()
            .field("UserID", .uuid, .required, .references(User.schema, "id"))
            .field("FriendID", .uuid, .required, .references(User.schema, "id"))
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema(UserFriend.schema).delete()
    }
}
