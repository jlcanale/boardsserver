//
//  UserMigration.swift
//  App
//
//  Created by Joseph Canale on 5/12/20.
//

import Vapor
import Fluent

struct UserMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(User.schema)
            .id()
            .field("FirstName", .string)
            .field("LastName", .string)
            .field("EmailAddress", .string)
            .field("FriendKey", .string)
            .field("AppleUserID", .string)
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema(User.schema).delete()
    }
}
