//
//  DeviceMigration.swift
//  App
//
//  Created by Joseph Canale on 5/13/20.
//

import Vapor
import Fluent

struct DeviceMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Device.schema)
            .id()
            .field("DeviceID", .string, .required)
            .field("UserID", .uuid, .required, .references(User.schema, "id"))
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Device.schema).delete()
    }
}
