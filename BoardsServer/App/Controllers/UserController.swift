//
//  FriendController.swift
//  App
//
//  Created by Joseph Canale on 5/1/20.
//

import Vapor
import Fluent

struct BoardsSession: Content {
    let token: String
    let user: User
}

final class UserController {

    /// Returns `Users`s.
    func getAll(_ req: Request) throws -> EventLoopFuture<[User]> {
        return User.query(on: req.db).all()
    }

    /// Returns a single `User`
    func get(_ req: Request) throws -> EventLoopFuture<User> {
        let user = try req.auth.require(User.self)
        return User.query(on: req.db).with(\.$friends).with(\.$devices).filter(\User.$id == user.id ?? UUID()).first().unwrap(or: Abort(.notFound))
    }

    /// Saves a decoded `User` to the database.
    func create(_ req: Request) throws -> EventLoopFuture<BoardsSession> {
        let user = try req.content.decode(User.self)
        var token: Token!
        user.friendKey = ShortCodeGenerator.getCode(ofLength: 6)

        return checkIfUserExists(user.appleUserID, req: req).flatMap { exists in
            guard !exists else {
                return req.eventLoop.future(error: UserError.userAlreadyExists)
            }
            return user.save(on: req.db)
                .flatMap {
                    guard let newToken = try? user.createToken(source: .signup) else {
                        return req.eventLoop.future(error: Abort(.internalServerError))
                    }
                    token = newToken
                    return token.save(on: req.db)
                }
            .flatMapThrowing {
                BoardsSession(token: token.value, user: user)
            }
        }
    }

    /// Updates a decoded `User` record in the database.
    func update(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let currentUser = try req.auth.require(User.self)
        let updatedUser = try req.content.decode(User.self)

        currentUser.firstName = updatedUser.firstName
        currentUser.lastName = updatedUser.lastName
        currentUser.emailAddress = updatedUser.emailAddress

        return currentUser.update(on: req.db).transform(to: .ok)
    }

    /// Deletes a parameterized `User`.
    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = try req.auth.require(User.self)
        return user.delete(on: req.db).transform(to: .ok)
    }

    func login(_ req: Request) throws -> EventLoopFuture<BoardsSession> {
        let user = try req.auth.require(User.self)
        let token = try user.createToken(source: .login)

        return token.save(on: req.db)
            .flatMapThrowing {
                BoardsSession(token: token.value, user: user)
            }
    }

    func addFriend(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = req.eventLoop.future(try req.auth.require(User.self))
        guard let friendKey = req.parameters.get("friendKey") else { throw Abort(.notFound) }

        let friend = User.query(on: req.db).filter(\.$friendKey == friendKey).first()
            .unwrap(or: Abort(.notFound))

        return user.and(friend).flatMap { (user, friend) in
            user.$friends.attach(friend, on: req.db)
        }.transform(to: .ok)
    }

    func removeFriend(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = req.eventLoop.future(try req.auth.require(User.self))
        guard let friendKey = req.parameters.get("friendKey") else { throw Abort(.notFound) }

        let friend = User.query(on: req.db).filter(\.$friendKey == friendKey).first()
            .unwrap(or: Abort(.notFound))

        return user.and(friend).flatMap { (user, friend) in
            user.$friends.detach(friend, on: req.db)
        }.transform(to: .ok)
    }

    func addDevice(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = try req.auth.require(User.self)
        guard let deviceID = req.parameters.get("deviceID") else { throw Abort(.notFound) }

        let userID = try user.requireID()
        let device = Device(deviceID: deviceID, userID: userID)

        return  Device.query(on: req.db).with(\.$user).filter(\.$deviceID == deviceID).first().map { existingDevice in
            if existingDevice == nil {
                _ = device.save(on: req.db)
            }
        }.transform(to: .ok)
    }

    func removeDevice(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = try req.auth.require(User.self)
        guard let deviceID = req.parameters.get("deviceID") else { throw Abort(.notFound) }

        let userID = try user.requireID()

        return Device.query(on: req.db)
            .with(\.$user)
            .filter(\.$deviceID == deviceID)
            .filter(\.$user.$id == userID)
            .first()
            .unwrap(or: Abort(.notFound))
            .flatMap { $0.delete(on: req.db) }
            .transform(to: .ok)
    }

    private func checkIfUserExists(_ appleUserID: String, req: Request) -> EventLoopFuture<Bool> {
        User.findByAppleUserID(appleUserID, on: req.db).map { $0 != nil}
    }

}

extension UserController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        //User Routes

        //Public
        //routes.get("", use: getAll)
        routes.post("", use: create)

        let appleProtected = routes.grouped(User.authenticator())
        appleProtected.post("login", use: login)

        //Protected
        let tokenProtected = routes.grouped(Token.authenticator())
        tokenProtected.get("tokenTest") { (req) -> String in
            _ = try req.auth.require(User.self)
            return "You're in like Flynn!"
        }
        tokenProtected.get("me", use: get)
        tokenProtected.put("me", use: update)
        tokenProtected.delete("me", use: delete)


        //Friend Routes
        tokenProtected.post("friends", ":friendKey", use: addFriend)
        tokenProtected.delete("friends", ":friendKey", use: removeFriend)

        //Device Routes
        tokenProtected.post("devices", ":deviceID", use: addDevice)
        tokenProtected.delete("devices", ":deviceID", use: removeDevice)
    }
}

