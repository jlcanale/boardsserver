//
//  APNSController.swift
//  BoardsServer
//
//  Created by Joseph Canale on 5/14/20.
//  Copyright © 2020 Joseph Canale. All rights reserved.
//

import Vapor
import Fluent

final class APNSController {

    func sendTest(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        _ = try req.auth.require(User.self)
        let deviceID = "8480982e28cc56808a4c39183781fdc78be9029f2647e28a4e7fd65811dba6de"
        return req.apns.send(.init(title: "Title", subtitle: "Subtitle", body: "Body"), to: deviceID).map { .ok }
    }

    func broadcastBoothLocation(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        _ = try req.auth.require(User.self)
        let boothBroadcast = try req.content.decode(BoothBroadcast.self)
        return try PushNotificationHelper.send(boothBroadcast, for: req).map { _ in .ok }
    }

    func boothBroadcastResponse(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        _ = try req.auth.require(User.self)
        let boothBroadcastResponse = try req.content.decode(BoothBroadcastResponse.self)
        return try PushNotificationHelper.send(boothBroadcastResponse, for: req)
    }
}

extension APNSController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let tokenProtected = routes.grouped(Token.authenticator())
        tokenProtected.get("test", use: sendTest)
        tokenProtected.post("broadcastBoothLocation", use: broadcastBoothLocation)
        tokenProtected.post("boothBroadcastResponse", use: boothBroadcastResponse)
    }
}

